import sys
import pickle


# sounds = {}

class trienode:
    def __init__(self):
        self.word = False
        self.children = {}
        self.visit = False
        self.value = ""


# takes in a list of syllables- inverted and loops and inserts to trie

def insertWord(breaklist, root, sentence):
    node = root
    for syllable in breaklist:
        if syllable not in node.children:
            node.children[syllable] = trienode()
            node.children[syllable].parent = node
        node = node.children[syllable]

    node.word = True
    node.value = sentence

def setNode(root, matcherList):
    node = root
    for syllable in matcherList:
        if syllable in node.children:
            node = node.children[syllable]
        else:
            break
    return node
# finds k closest matching suffixs

def find(k, root, matcherList, result):
    node = setNode(root, matcherList)
    while len(result) < k:
        if node is not root:
            checkchildren(node, result, k)
            node = node.parent
            node.visit = True
    for something in result:
        s = something.strip()
        print(s)




def checkchildren(node, result, k):
    if len(result) < k:
        for values in node.children.values():

            if values.visit == False:
                if values.word == True:
                    # print("k is: ")
                    # print(k)
                    if len(result) >= k:
                        break
                    else:
                        #print(k)
                        result.append(values.value)

                        #print(result)
                        values.visit = True
                        node = values
                        if len(result) > k:
                            break
                else:
                    values.visit = True
                    node = values
                checkchildren(node, result, k)



def fileparse(inputfilename, sounds):
    cmu = open(inputfilename)
    for line in cmu:
        if ";;;" not in line:
            temp = line.split(" ", 1)
            word = temp[0].lower()
            sentence = temp[1].split()
            temp = []
            for something in sentence:
                if len(something) > 2:
                    something = something[0:2]
                temp.append(something)
            sounds[word] = temp

            # print(word)
            # print( " :")
            # print( sounds[word])

    cmu.close()


#    print(sounds["tactful"])

def sentenceparse(inputFile, root, sounds):
    sentencefile = open(inputFile)
    for line in sentencefile:
        temper = []
        temp = line.split()

        #        print(temp)

        for word in temp:
            temper = temper + sounds[word]
        temper.reverse()

        #            print(temper)

        insertWord(temper, root, line)
    sentencefile.close()


def main():
    sounds = {}
    pronunciationdictionaryFileName = sys.argv[1]
    sentenceFile = sys.argv[2]
    k = int(sys.argv[3])
    words = sys.argv[4]
    words.strip()
    wordList = words.split("_")
    matcherList = []

    root = trienode()
    fileparse(pronunciationdictionaryFileName, sounds)
    sentenceparse(sentenceFile, root, sounds)
    result = []

    #print("I am here")

    #       print(root.children)

    for word in wordList:
        matcherList = matcherList + sounds[word]
    matcherList.reverse()

    #print(matcherList)
    #print("reached find")
    find(k, root, matcherList, result)


main()
